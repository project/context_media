<?php

/**
 * @file
 * Context Media context reaction plugin.
 */

/**
 * Context media reaction plugin class.
 */
class context_media_context_reaction extends context_reaction {
  /**
   * {@inheritdoc}
   */
  public function options_form($context) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function options_form_submit($values) {
    // Remove helper elements.
    unset($values['add_element']);
    unset($values['remove_element']);

    // Save only non-empty values.
    foreach ($values as $delta => $value) {
      if ($this->option_value_is_empty($value)) {
        unset($values[$delta]);
      }
      else {
        // Save media URI instead of fid.
        $file = file_load($value['media']);
        if (!$file) {
          unset($values[$delta]);
        }
        $values[$delta]['media'] = $file->uri;
      }
    }

    return $values;
  }

  /**
   * Helper to check that option value is empty.
   */
  protected function option_value_is_empty($value) {
    return empty($value['media']);
  }

  /**
   * {@inheritdoc}
   */
  public function execute(&$vars) {
    $contexts = $this->get_contexts();
    $regions = element_children($vars);
    foreach ($contexts as $context) {
      if (!empty($context->reactions[$this->plugin])) {
        $regions_settings = $this->get_region_settings($context);
        foreach ($regions_settings as $region_name => $region_settings) {
          $region_name = $this->region_name_key_extract($region_name);
          if (array_key_exists($region_name, $regions) || $region_settings['region_force']) {
            $vars[$region_name]['context_media'] = $this->render_media($region_settings);
          }
        }
      }
    }
  }

  /**
   * Helper to get region settings for current context.
   *
   * @param object $context
   *   Context object.
   * @param string $region_name
   *   Optional region machine name. When not specified, settings for all
   *   regions set in provided context are returned. When specified, only
   *   settings for provided context region are returned if set, otherwise
   *   empty array is returned.
   *
   * @return array
   *   Array of settings for all regions, keyed by region name, if $region_name
   *   is not set. If $region_name is set, return settings for current region
   *   name if they were set in provided context, empty array otherwise.
   */
  protected function get_region_settings($context, $region_name = NULL) {
    $region_settings = array();

    $all_settings = $this->fetch_from_context($context, 'values');

    // Last settings values override first ones for specified region name.
    foreach ($all_settings as $settings) {
      $region_settings[$settings['region']] = $settings;
    }

    return !is_null($region_name) ? (isset($region_settings[$region_name]) ? $region_settings[$region_name] : array()) : $region_settings;
  }

  /**
   * Helper to render media element.
   *
   * @param array $settings
   *   Array of settings from context settings for current region.
   *
   * @return string
   *   Rendered HTML for curen media element.
   */
  protected function render_media($settings) {
    $render_array = array(
      '#prefix' => '<div class="context-media">',
      '#suffix' => '</div>',
      '#weight' => -99999,
    );

    $uri = $settings['media'];

    if (!$uri && !file_valid_uri($uri)) {
      return $render_array;
    }

    // Render using formatter.
    $op = 'view';

    $formatter = field_info_formatter_types($settings['formatter_type']);

    // Skip formatters with unsupported field types.
    $valid_type = array_intersect(_context_media_supported_field_types(), $formatter['field types']);
    if (empty($valid_type)) {
      return $render_array;
    }

    $function = $formatter['module'] . '_field_formatter_' . $op;
    if (function_exists($function)) {
      $items = array(array('uri' => $uri));

      $display = array(
        'type' => $settings['formatter_type'],
        'settings' => $settings['formatter_settings'],
      );

      $results = $function(NULL, NULL, NULL, NULL, NULL, $items, $display);

      $render_array += $results;
    }

    return $render_array;
  }

  /**
   * Helper to get current theme.
   */
  protected function get_current_theme() {
    if (!empty($GLOBALS['user']->theme)) {
      $current_theme = $GLOBALS['user']->theme;
    }
    elseif (!empty($GLOBALS['custom_theme'])) {
      $current_theme = $GLOBALS['custom_theme'];
    }
    else {
      $current_theme = $GLOBALS['theme'] ? $GLOBALS['theme'] : variable_get('theme_default', 'garland');
    }

    return $current_theme;
  }

  /**
   * Helper to create theme-based region name key.
   */
  protected function region_name_key_get($region_name, $theme = NULL) {
    if (is_null($theme)) {
      $theme = $this->get_current_theme();
    }

    return $theme . '|' . $region_name;
  }

  /**
   * Helper to extract region name key.
   */
  protected function region_name_key_extract($region_name_key) {
    if (strpos($region_name_key, '|') !== FALSE) {
      list(, $region_name) = explode('|', $region_name_key);
    }
    else {
      $region_name = $region_name_key;
    }

    return $region_name;
  }
}
